package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Local;

import com.esprit.ejb.persistence.Solution;

@Local
public interface SolutionManagmentLocal {
	
	public void  createSolution(Solution solution);
	public void  deleteSolution(Solution solution);
	public void  updateSolution(Solution solution);
	public Solution getSolutiongeById(int idSolution);
	public List<Solution> getAllSolution();


}
