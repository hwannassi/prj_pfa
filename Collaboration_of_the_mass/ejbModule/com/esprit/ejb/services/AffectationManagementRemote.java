package com.esprit.ejb.services;

import javax.ejb.Remote;

import com.esprit.ejb.persistence.Invitation;
import com.esprit.ejb.persistence.User;

@Remote
public interface AffectationManagementRemote {

	public void createAffectation(User user, Invitation invitation);

}
