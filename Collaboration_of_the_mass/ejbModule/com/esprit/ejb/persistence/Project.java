package com.esprit.ejb.persistence;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Project implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Project() {
		// TODO Auto-generated constructor stub
	}

	private int idProject;
	private String description;
	private byte [] avatar;
	private String challengetype;
	private String title;
	private String award;
	private String startDate ;
	private String deadeLine;
	private boolean contest;
	private String otherSkills="true";
	private String hardwareSkills="true";
	private String softwareSkills="true";
	private String phpSkills="true";
	private String javaSkills="true";
	private String netSkills="true";
	private String cSkills="true";
	private Admin adminc;
	public String getOtherSkills() {
		return otherSkills;
	}

	public void setOtherSkills(String otherSkills) {
		this.otherSkills = otherSkills;
	}

	public String getHardwareSkills() {
		return hardwareSkills;
	}

	public void setHardwareSkills(String hardwareSkills) {
		this.hardwareSkills = hardwareSkills;
	}

	public String getSoftwareSkills() {
		return softwareSkills;
	}

	public void setSoftwareSkills(String softwareSkills) {
		this.softwareSkills = softwareSkills;
	}

	public String getPhpSkills() {
		return phpSkills;
	}

	public void setPhpSkills(String phpSkills) {
		this.phpSkills = phpSkills;
	}

	public String getJavaSkills() {
		return javaSkills;
	}

	public void setJavaSkills(String javaSkills) {
		this.javaSkills = javaSkills;
	}

	public String getNetSkills() {
		return netSkills;
	}

	public void setNetSkills(String netSkills) {
		this.netSkills = netSkills;
	}

	public String getcSkills() {
		return cSkills;
	}

	public void setcSkills(String cSkills) {
		this.cSkills = cSkills;
	}

	private User use;
	
	private Set<Solution> soluti;
	private Set<Invitation> invitations;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getIdProject() {
		return this.idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



	public String getStartDate() {
		return this.startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getDeadeLine() {
		return this.deadeLine;
	}

	public void setDeadeLine(String deadeLine) {
		this.deadeLine = deadeLine;
	}

	public boolean getContest() {
		return this.contest;
	}

	public void setContest(boolean contest) {
		this.contest = contest;
	}

	@XmlTransient
	@OneToMany(mappedBy = "challengee", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Solution> getSoluti() {
		return soluti;
	}

	public void setSoluti(Set<Solution> soluti) {
		this.soluti = soluti;
	}

	public Project(String title, String award,
			String startDate, String deadeLine, boolean contest) {
		super();
		this.title = title;
		this.award = award;
		this.startDate = startDate;
		this.deadeLine = deadeLine;
		this.contest = contest;
	}

	@ManyToOne
	@XmlInverseReference(mappedBy="ch")
	public User getUse() {
		return use;
	}

	public void setUse(User use) {
		this.use = use;
	}

	@XmlTransient
	@OneToMany(mappedBy = "challengeinvi", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Invitation> getInvitations() {
		return invitations;
	}

	public void setInvitations(Set<Invitation> invitations) {
		this.invitations = invitations;
	}

	public String getChallengetype() {
		return challengetype;
	}

	public void setChallengetype(String challengetype) {
		this.challengetype = challengetype;
	}
@ManyToOne
	public Admin getAdminc() {
		return adminc;
	}

	public void setAdminc(Admin adminc) {
		this.adminc = adminc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte [] getAvatar() {
		return avatar;
	}

	public void setAvatar(byte [] avatar) {
		this.avatar = avatar;
	}

	public String getAward() {
		return award;
	}

	public void setAward(String award) {
		this.award = award;
	}

}
