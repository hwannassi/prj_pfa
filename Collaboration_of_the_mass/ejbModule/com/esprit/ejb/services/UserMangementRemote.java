package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.esprit.ejb.persistence.User;

@WebService
@javax.xml.ws.BindingType(value="http://java.sun.com/xml/ns/jaxws/2003/05/soap/bindings/HTTP/" )
@Remote
public interface UserMangementRemote {
	@WebMethod
	public void createUser(User user);

	@WebMethod
	public void deleteUser(@WebParam User user);

	@WebMethod
	public void updateUser(User user);

	@WebMethod
	public User getUserById(int idUser);

	@WebMethod
	public List<User> getAllUser();

	@WebMethod
	public User findUserby(String login, String password);

	@WebMethod
	public User login(String username, String password);

}
