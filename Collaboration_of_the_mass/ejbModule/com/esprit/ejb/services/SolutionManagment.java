package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.esprit.ejb.persistence.Project;
import com.esprit.ejb.persistence.Solution;

/**
 * Session Bean implementation class SolutionManagment
 */
@Stateless
public class SolutionManagment implements SolutionManagmentRemote, SolutionManagmentLocal {

	@PersistenceContext
    EntityManager em;
    public SolutionManagment() {
    	
    }

	@Override
	public void createSolution(Solution solution) {
		em.persist(solution);
		em.flush();
		
	}

	@Override
	public void deleteSolution(Solution solution) {
		em.remove(em.merge(solution));
		
	}

	@Override
	public void updateSolution(Solution solution) {
		em.merge(solution);
		
	}

	@Override
	public Solution getSolutiongeById(int idSolution) {
		Solution solution=em.find(Solution.class,idSolution);
		return solution;
	}

	@Override
	public List<Solution> getAllSolution() {
		Query query=em.createQuery("select e from Solution e");
		return query.getResultList();
	}

}
