package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.esprit.ejb.persistence.Project;
import com.esprit.ejb.persistence.User;

/**
 * Session Bean implementation class ProjectManagement
 */

@Stateless
public class ProjectManagement implements ProjectManagementRemote, ProjectManagementLocal {
@PersistenceContext
    EntityManager em;
    public ProjectManagement() {
        
    }

	@Override
	
	public void createProject(Project project) {
		em.persist(project);
		
		
	}

	@Override

	public void deleteProject(Project project) {
		em.remove(em.merge(project));
	}

	@Override

	public void updateProject(Project project) {
	em.merge(project);
		
	}

	@Override

	public Project getProjectById(int id_Project) {
		Project project=em.find(Project.class,id_Project);
		return project;
	}



	@Override

	public List<Project> getAllProjects() {
		Query query=em.createQuery("select e from Project e");
		return query.getResultList();
	}

	@Override
	
	public List<Project> getAllTrueProjects() {
		Query query=em.createQuery("select e from Project e where contest = true");
		return query.getResultList();
	}

	@Override
	public List<Project> getAllTrueProjectsuser(User user) {
		Query query=em.createQuery("select e from Project e where user= :user");
		  query.setParameter("user", user);
		return query.getResultList();
	}


	


}
