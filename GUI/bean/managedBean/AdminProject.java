package managedBean;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import com.esprit.ejb.persistence.Project;

import com.esprit.ejb.services.ProjectManagementRemote;
import com.esprit.ejb.services.UserMangementRemote;
@ManagedBean(name = "adminProject")
@SessionScoped
public class AdminProject {

	@EJB
	private UserMangementRemote remoteUser;

	@EJB
	private ProjectManagementRemote remoteChallange;

	private Project project=new Project();
	
	private DataModel dataModelM = new ListDataModel();



	
	private DataModel dataModelC = new ListDataModel();
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();

	

	public DataModel getDataModel() {
		dataModelC.setWrappedData(remoteChallange.getAllProjects());
		return dataModelC;
	}

	public void setDataModel(DataModel dataModelC) {
		this.dataModelC = dataModelC;
	}



	
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void updateChallengeTrue(ActionEvent event){
		
		 project= (Project) dataModelC.getRowData();
		 project.setContest(true);
		 remoteChallange.updateProject(project);
			dataModelC.setWrappedData(remoteChallange.getAllProjects());
		
			try {
				ec.redirect("Admin.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	}
	public void updateChallengeFalse(ActionEvent event){
		
		 project= (Project) dataModelC.getRowData();
		 project.setContest(false);
		 remoteChallange.updateProject(project);
			dataModelC.setWrappedData(remoteChallange.getAllProjects());
		
			try {
				ec.redirect("Admin.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
								
	}
	
	
}
