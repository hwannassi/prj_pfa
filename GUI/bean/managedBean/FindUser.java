package managedBean;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;


import com.esprit.ejb.persistence.User;
import com.esprit.ejb.services.UserMangementRemote;
@ManagedBean(name="findUser")
@SessionScoped
public class FindUser implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	UserMangementRemote remoteU;
	
	private User user=new User();

public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


public void findUser(ActionEvent event,int idUser){
	
	remoteU.getUserById(idUser);
	
	
}
	


public FindUser() {
		
	}
}
