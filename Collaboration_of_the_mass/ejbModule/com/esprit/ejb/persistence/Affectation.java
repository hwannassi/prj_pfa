package com.esprit.ejb.persistence;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: AffectationInvitation
 *
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Affectation implements Serializable {

	/*
	private int idAffectation;
	private User user;
	private Invitation invitation;
	private static final long serialVersionUID = 1L;

	public Affectation() {
		super();
	}   
	@Id  
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdAffectation() {
		return this.idAffectation;
	}

	public void setIdAffectation(int idAffectation) {
		this.idAffectation = idAffectation;
	}   
	@ManyToOne
	@XmlInverseReference(mappedBy="affectations")
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}   
	@ManyToOne
	@XmlInverseReference(mappedBy="affectations")
	public Invitation getInvitation() {
		return this.invitation;
	}

	public void setInvitation(Invitation invitation) {
		this.invitation = invitation;
	}
   */
}
