package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Remote;

import com.esprit.ejb.persistence.Project;
import com.esprit.ejb.persistence.User;

@Remote
public interface ProjectManagementRemote {

	public void createProject(Project project);

	public void deleteProject(Project project);

	public void updateProject(Project project);

	public Project getProjectById(int idChallenge);

	public List<Project> getAllProjects();

	public List<Project> getAllTrueProjects();

	public List<Project> getAllTrueProjectsuser(User user);

}
