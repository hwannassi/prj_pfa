package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Remote;


import com.esprit.ejb.persistence.Solution;

@Remote
public interface SolutionManagmentRemote {
	public void  createSolution(Solution solution);
	public void  deleteSolution(Solution solution);
	public void  updateSolution(Solution solution);
	public Solution getSolutiongeById(int idSolution);
	public List<Solution> getAllSolution();
	

}
