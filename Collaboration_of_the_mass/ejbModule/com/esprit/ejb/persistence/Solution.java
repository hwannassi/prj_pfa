package com.esprit.ejb.persistence;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Solution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Solution() {
		// TODO Auto-generated constructor stub
	}

	private int idSolution;
	private byte[] test;
	private String description;
	private boolean acceptCondition;

	private byte[] solutionFile;
	private String filename;

	private int feedbacks;
	private Project challengee;
	private User users;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getIdSolution() {
		return idSolution;
	}

	public void setIdSolution(int idSolution) {
		this.idSolution = idSolution;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isAcceptCondition() {
		return acceptCondition;
	}

	public void setAcceptCondition(boolean acceptCondition) {
		this.acceptCondition = acceptCondition;
	}

	@ManyToOne
	//@XmlInverseReference(mappedBy = "soluti")
	public Project getChallengee() {
		return challengee;
	}

	public void setChallengee(Project challengee) {
		this.challengee = challengee;
	}

	@ManyToOne
	@XmlInverseReference(mappedBy = "sol")
	public User getUsers() {
		return users;
	}

	public void setUsers(User users) {
		this.users = users;
	}

	public int getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(int feedbacks) {
		this.feedbacks = feedbacks;
	}

	@Column(columnDefinition = "LONGBLOB")
	
	public byte[] getSolutionFile() {
		return solutionFile;
	}

	public void setSolutionFile(byte[] solutionFile) {
		this.solutionFile = solutionFile;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(length=1000004857) 
	public byte[] getTest() {
		return test;
	}

	public void setTest(byte[] test) {
		this.test = test;
	}

}
