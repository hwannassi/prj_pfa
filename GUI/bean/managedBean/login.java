package managedBean;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Null;

import com.esprit.ejb.persistence.Project;
import com.esprit.ejb.persistence.Invitation;
import com.esprit.ejb.persistence.User;
import com.esprit.ejb.persistence.User.LoginStatus;
import com.esprit.ejb.services.ProjectManagementRemote;
import com.esprit.ejb.services.InvitationManagementRemote;
import com.esprit.ejb.services.UserMangementRemote;
@ManagedBean(name = "loginbean")
@SessionScoped
public class login  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public login() {
		// TODO Auto-generated constructor stub
	}
	
	

	@EJB
	private UserMangementRemote remoteUser;
	
	public static int idlogin;
	
	public DataModel getDataModel() {
		dataModel.setWrappedData(remoteUser.getAllUser());
		return dataModel;
	}

	public void setDataModel(DataModel dataModel) {
		this.dataModel = dataModel;
	}

	public String getLoginP() {
		return loginP;
	}

	public void setLoginP(String loginP) {
		this.loginP = loginP;
	}

	public String getPassP() {
		return passP;
	}

	public void setPassP(String passP) {
		this.passP = passP;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	
	private User user=new User();
	public static User userlogin=new User();




	private String loginP;
	private String passP;
	
	private DataModel dataModel=new ListDataModel();
	
	private List<User> list=new ArrayList();
	private List<User> list2=new ArrayList();
	
	public void Apply(ActionEvent event){
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		if(loginP!=null && passP!=null)
	{
		 user = remoteUser.findUserby(loginP,passP);

	if (loginP.equals(user.getLogin()) && passP.equals(user.getPassword())){
		idlogin=user.getIdUser();
		try {
			ec.redirect("Create.xhtml");
			} catch (Exception e) {
		
		
	
		}
		}else if(loginP.equals("admin") && passP.equals("admin")){
			try {
				ec.redirect("Admin.xhtml");
				} catch (Exception e) {
			
			
		
			}
		}
	   // user = remoteUser.findUserby(loginP);
		
	
		}else{
			
			
		}
		
	
	}
		        
		    
		    
		 
				
		
		
	
	public void loginn() {
	    FacesContext context = FacesContext.getCurrentInstance();
	    HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

	    try {
	        request.login(loginP, passP);
	     //   user = (remoteUser).findUserby(loginP);
	    } catch (ServletException e) {
	        // Handle unknown username/password in request.login().
	        context.addMessage(null, new FacesMessage("Unknown login"));
	    }
	
	
	}
	
	 public String doLogin(){
		 ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        userlogin = remoteUser.login(loginP, passP);
         FacesMessage message = new FacesMessage("");

         if(userlogin.getLoginStatus().equals(LoginStatus.SUCCESSFUL))
			try {
				ec.redirect("Home.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else if(loginP.equals("admin") && passP.equals("admin")){
			try {
				ec.redirect("Admin.xhtml");
				} catch (Exception e) {
			
			
		
			}
		}
         else
                message = new FacesMessage("Invalid username or password");

         FacesContext.getCurrentInstance().addMessage(null, message);
         return null;

  }
	
}
