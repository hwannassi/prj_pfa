package managedBean;



	import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.UploadedFile;

	import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;
import com.dropbox.client2.session.WebAuthSession;
import com.snowtide.pdf.OutputTarget;
import com.snowtide.pdf.PDFTextStream;

	/**
	 * A very basic dropbox example.
	 * 
	 * @author mjrb5
	 */
	public class DropBox {

	        public static String APP_KEY = "zsr3nxkfl91erm9";
	        public static String DEFAULT_PATH = "becher";
	        public static String APP_SECRET = "r9sngrgxa7zthof";
	        private static final AccessType ACCESS_TYPE = AccessType.APP_FOLDER;
	        private static DropboxAPI<WebAuthSession> mDBApi;
	        private static Boolean authentified = Boolean.TRUE;
	        public static String TOKEN_KEY = "t8e7i4fohf36qro";
	        public static String TOKEN_SECRET = "s5aer2acpxtmtt9";
	        public static Boolean validate = Boolean.FALSE;

	        public static void main(String[] args) throws Exception {

	                AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
	                WebAuthSession session = new WebAuthSession(appKeys, ACCESS_TYPE);

	               

	              //  DropBox db = new DropBox();
	             // File uploadFile = new File("C://Atelier 1.pdf");
	         
	            // db.putFile("C:/", uploadFile);
	              //  db.DEFAULT_PATH = "dropbox";
	              //  db.getListofFiles("demo/database/save", "db4o");

	                // File downloadFile = new File("C:/downloadFile.txt");
	                // db.getFile("database/chat.jsp", downloadFile);

	        }

	        public Entry putFile(UploadedFile file) throws IOException,
	                        DropboxException {
	                if (!validate)
	                        validate();
	              /*  PDFTextStream stream = new PDFTextStream(file);
				    StringBuffer sb = new StringBuffer(1024);
				   
				    OutputTarget tgt = OutputTarget.forBuffer(sb);
				    stream.pipe(tgt);
				    stream.close();
				    String fileContents =sb.toString();*/
	                ByteArrayInputStream inputStream = new ByteArrayInputStream(file.getContents());
				    Entry newEntry = mDBApi.putFile(file.getFileName(), inputStream,(int)file.getSize(), null, null);
	                return newEntry;

	        }

	        public void deleteFile(String path, String file) {
	                if (!validate)
	                        validate();
	                try {
	                        mDBApi.delete(DEFAULT_PATH + "/" + path + "/" + file);
	                } catch (DropboxException e) {
	                        e.printStackTrace();
	                }

	        }

	        public List<String> getListofFiles(String path, String pattern) {
	                if (!validate)
	                        validate();
	                List<String> retour = new ArrayList<String>();
	                List<Entry> liste = new ArrayList<Entry>();
	                ;
	                try {

	                        liste = mDBApi.search("/", pattern, 0, false);
	                } catch (DropboxException e) {
	                        e.printStackTrace();
	                }
	                for (Entry entry : liste) {
	                        if (entry.path.contains(path)) {
	                                retour.add(entry.fileName());
	                        }
	                }
	                return retour;

	        }

	        public void getFile(String fileNameWithPath, String destinationFile)
	                        throws DropboxException, IOException {
	                if (!validate)
	                        validate();
	                try {
	                        System.out.println("FileOutPutStream " + destinationFile);
	                        FileOutputStream fos = new FileOutputStream(destinationFile);
	                        System.out.println("mDBApi.getFile " + DEFAULT_PATH + "/"
	                                        + fileNameWithPath);
	                        mDBApi.getFile(DEFAULT_PATH + "/" + fileNameWithPath, null, fos,
	                                        null);
	                        fos.close();
	                } catch (Exception e) {
	                        e.printStackTrace();
	                }

	        }

	        public void validate() {
	                AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
	                WebAuthSession session = new WebAuthSession(appKeys, ACCESS_TYPE);
	                session.setAccessTokenPair(new AccessTokenPair(TOKEN_KEY, TOKEN_SECRET));
	                mDBApi = new DropboxAPI<WebAuthSession>(session);
	                validate = Boolean.TRUE;
	        }

	        public DropBox() {
	        	   AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
	                WebAuthSession session = new WebAuthSession(appKeys, ACCESS_TYPE);
	        }

}
