package managedBean;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.esprit.ejb.persistence.User;
import com.esprit.ejb.services.UserMangementRemote;


@ManagedBean(name="beanregister")
@SessionScoped
public class Register implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	UserMangementRemote remote;
	
	private User user= new User();

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void createUser (ActionEvent event)
	{
		
		if(isPhpvalue1()==true){
			user.setPhpSkills("PHP");	
		}
		if(isJavavalue4()==true){
			user.setJavaSkills("JAVA");
		}
		if(isNetvalue2()==true){
			user.setNetSkills("NET");
		}if(isCvalue3()==true){
			user.setcSkills("C++");	
		} if(isSoftwaretvalue5()==true){
			user.setSoftwareSkills("SOFTWARE");	
		}if(isHardwarevalue6()==true){
			user.setHardwareSkills("HARDWARE");
			
		}
		remote.createUser(user);
		setNetvalue2(false);
		setCvalue3(false);
		setNetvalue2(false);
		setSoftwaretvalue5(false);
		setPhpvalue1(false);
		setHardwarevalue6(false);		
		user= new User();
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		try {
			ec.redirect("Auth.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	    private boolean phpvalue1;  
	    private boolean netvalue2; 
	    private boolean cvalue3;
	    private boolean javavalue4;
	    private boolean softwaretvalue5;
	    private boolean hardwarevalue6;
	  
	    
	  
	    public boolean isPhpvalue1() {
			return phpvalue1;
		}

		public void setPhpvalue1(boolean phpvalue1) {
			this.phpvalue1 = phpvalue1;
		}

		public boolean isNetvalue2() {
			return netvalue2;
		}

		public void setNetvalue2(boolean netvalue2) {
			this.netvalue2 = netvalue2;
		}

		public boolean isCvalue3() {
			return cvalue3;
		}

		public void setCvalue3(boolean cvalue3) {
			this.cvalue3 = cvalue3;
		}

		public boolean isJavavalue4() {
			return javavalue4;
		}

		public void setJavavalue4(boolean javavalue4) {
			this.javavalue4 = javavalue4;
		}

		public boolean isSoftwaretvalue5() {
			return softwaretvalue5;
		}

		public void setSoftwaretvalue5(boolean softwaretvalue5) {
			this.softwaretvalue5 = softwaretvalue5;
		}

		public boolean isHardwarevalue6() {
			return hardwarevalue6;
		}

		public void setHardwarevalue6(boolean hardwarevalue6) {
			this.hardwarevalue6 = hardwarevalue6;
		}

		public void addMessage() {  
	       // String summary = netvalue2 ? "Checked" : "Unchecked";  
	     String summary4 = javavalue4 ? "Checked" : "Unchecked";  
	       
	  
	     //   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
	      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary4));  
	       
	    }  
		public void addMessagephp() {  
		      String summary = phpvalue1 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		public void addMessagenet() {  
		      String summary = netvalue2 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		public void addMessagec() {  
		      String summary = cvalue3 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		public void addMessagehard() {  
		      String summary = hardwarevalue6 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		public void addMessagesoft() {  
		      String summary = softwaretvalue5 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
}
