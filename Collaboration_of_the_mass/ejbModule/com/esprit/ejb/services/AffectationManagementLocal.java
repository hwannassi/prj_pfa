package com.esprit.ejb.services;

import javax.ejb.Local;

import com.esprit.ejb.persistence.Invitation;
import com.esprit.ejb.persistence.User;

@Local
public interface AffectationManagementLocal {
	public void  createAffectation(User user,Invitation invitation);
}
