package com.esprit.ejb.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implimport javax.xml.bind.annotation.XmlAccessorType; ementation class
 * for Entity: User
 * 
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User implements Serializable {

	@Temporal(TemporalType.DATE)
	@Column(name = "LAST_LOGIN_DATE")
	private Date lastLoginDate;

	public enum LoginStatus {
		SUCCESSFUL, INVALID_USERNAME_OR_PASSWORD, INACTIVE
	}

	@Transient
	private LoginStatus loginStatus;

	public LoginStatus getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(LoginStatus loginStatus) {
		this.loginStatus = loginStatus;
	}

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "REGISTRATION_DATE")
	private Date registrationDate;
	@XmlID
	private String refId;
	public String getRefId() {
		refId=String.valueOf(getIdUser()) ;
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	private int idUser;
	private String login;
	private String firstName;
	private String lastName;
	private String password;
	private String interestField;
	private String email;
	private int phone;
	private String adress;
	private String otherSkills="false";
	private String hardwareSkills="false";
	private String softwareSkills="false";
	private String phpSkills="false";
	private String javaSkills="false";
	private String netSkills="false";
	private String cSkills="false";
	

	 public String getOtherSkills() {
		return otherSkills;
	}

	public void setOtherSkills(String otherSkills) {
		this.otherSkills = otherSkills;
	}

	public String getPhpSkills() {
		return phpSkills;
	}

	public void setPhpSkills(String phpSkills) {
		this.phpSkills = phpSkills;
	}

	public String getJavaSkills() {
		return javaSkills;
	}

	public void setJavaSkills(String javaSkills) {
		this.javaSkills = javaSkills;
	}

	public String getNetSkills() {
		return netSkills;
	}

	public void setNetSkills(String netSkills) {
		this.netSkills = netSkills;
	}

	public String getcSkills() {
		return cSkills;
	}

	public void setcSkills(String cSkills) {
		this.cSkills = cSkills;
	}

	private Set<Invitation> recievedinvitation;
	// private Set<Invitation> sentinvitations;
	//private Set<Affectation> affectations;
	private Set<Project> ch;
	private Set<Solution> sol;

	private static final long serialVersionUID = 1L;

	public User() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@XmlTransient
	@OneToMany(mappedBy = "use", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	public Set<Project> getCh() {
		return ch;
	}

	public void setCh(Set<Project> ch) {
		this.ch = ch;
	}

	@XmlTransient
	@OneToMany(mappedBy = "users", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Solution> getSol() {
		return sol;
	}

	public void setSol(Set<Solution> sol) {
		this.sol = sol;
	}

	public String getInterestField() {
		return interestField;
	}

	public void setInterestField(String interestField) {
		this.interestField = interestField;
	}

	 @ManyToMany(mappedBy="invited",fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	 @XmlInverseReference(mappedBy="invited")
	public Set<Invitation> getRecievedinvitation() { 
		return recievedinvitation;
		}
	  public void setRecievedinvitation(Set<Invitation> recievedinvitation) {
	  this.recievedinvitation = recievedinvitation; }
	  
	 /* @OneToMany(mappedBy="inviter", fetch = FetchType.EAGER) public
	 * Set<Invitation> getSentinvitations() { return sentinvitations; } public
	 * void setSentinvitations(Set<Invitation> sentinvitations) {
	 * this.sentinvitations = sentinvitations; }
	 */
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getSoftwareSkills() {
		return softwareSkills;
	}

	public void setSoftwareSkills(String softwareSkills) {
		this.softwareSkills = softwareSkills;
	}

	public String getHardwareSkills() {
		return hardwareSkills;
	}

	public void setHardwareSkills(String hardwareSkills) {
		this.hardwareSkills = hardwareSkills;
	}

	/*@XmlTransient
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Affectation> getAffectations() {
		return affectations;
	}

	public void setAffectations(Set<Affectation> affectations) {
		this.affectations = affectations;
	}

	public void manageAffectation(Affectation affectation) {
		this.affectations.add(affectation);
		affectation.setUser(this);
	}*/

}
