package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Local;

import com.esprit.ejb.persistence.Invitation;
import com.esprit.ejb.persistence.User;

@Local
public interface InvitationManagementLocal {
	public void  createInvitation(Invitation invitation);
	public void  deleteInvitation(Invitation invitation);
	public void  updateInvitation(Invitation invitation);
	public Invitation getInvitationById(int idInvitation);
	public List<Invitation> getAllInvitations();
	public List <User> getAllInvitedUser();
	public List <User> getAllUserInvitaionById(int id);

}
