package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.esprit.ejb.persistence.Invitation;
import com.esprit.ejb.persistence.User;

/**
 * Session Bean implementation class InvitationManagement
 */
@Stateless
public class InvitationManagement implements InvitationManagementRemote, InvitationManagementLocal {
	@PersistenceContext
    EntityManager em;
    public InvitationManagement() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void createInvitation(Invitation invitation) {
		em.persist(invitation);
		
		
		
	}

	@Override
	public void deleteInvitation(Invitation invitation) {
		em.remove(em.merge(invitation));
		
	}

	@Override
	public void updateInvitation(Invitation invitation) {
		em.merge(invitation);
		
	}

	@Override
	public Invitation getInvitationById(int idInvitation) {
		Invitation invi=em.find(Invitation.class,idInvitation);
		return invi;
	}

	@Override
	public List<Invitation> getAllInvitations() {
		Query query=em.createQuery("select e from Invitation e");
		return query.getResultList();
	}

	@Override
	public List<User> getAllInvitedUser() {
		Query query=em.createQuery("select e.invited from Invitation e");
		return query.getResultList();
	}

	@Override
	public List<User> getAllUserInvitaionById(int idUser) {
		Query query=em.createQuery("select e.invited from Invitation e");
		//query.setParameter("idInvitation", idUser);
		return query.getResultList();
	}

}
