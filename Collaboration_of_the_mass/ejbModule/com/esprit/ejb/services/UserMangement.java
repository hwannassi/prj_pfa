package com.esprit.ejb.services;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.esprit.ejb.persistence.User;
import com.esprit.ejb.persistence.User.LoginStatus;

/**
 * Session Bean implementation class UserMangement
 */
@WebService

@Stateless
public class UserMangement implements UserMangementRemote, UserMangementLocal {

	@PersistenceContext
	EntityManager em;

	public UserMangement() {

	}

	@Override
	public void createUser(User user) {
		em.persist(user);

	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public User getUserById(int idUser) {
		User user = em.find(User.class, idUser);
		return user;
	}

	@Override
	public List<User> getAllUser() {
		Query query = em.createQuery("select e from User e");
		return query.getResultList();
	}

	@Override
	public User findUserby(String login, String password) {

		Query query = em.createQuery("select e from User e where login='"
				+ login + "' AND password='" + password + "'");
		User user = (User) query.getSingleResult();
		return user;

	}

	@Override
	public User login(String login, String password) {
		User user = new User();
		boolean isLoginSuccessful = false;

		try {
			String userQuery = "select u from User u where upper(login) = upper(:login)";
			Query query = em.createQuery(userQuery);
			query.setParameter("login", login);

			try {
				user = (User) query.getSingleResult();
				user.setLoginStatus(LoginStatus.SUCCESSFUL);

				if (user.getPassword() != null
						&& !user.getPassword().equals(password))
					user.setLoginStatus(LoginStatus.INVALID_USERNAME_OR_PASSWORD);

			} catch (NoResultException ex) {
				user.setLoginStatus(LoginStatus.INVALID_USERNAME_OR_PASSWORD);
			}

			isLoginSuccessful = user.getLoginStatus().equals(
					LoginStatus.SUCCESSFUL);

			if (isLoginSuccessful) {
				user.setLastLoginDate(user.getLastLoginDate());
				user.setLastLoginDate(new Date());

				em.merge(user);

			}

		} catch (RuntimeException ex) {
			throw ex;
		} finally {

		}
		return user;
	}

	@Override
	@WebMethod
	public void deleteUser(@WebParam User user) {

		em.remove(em.merge(user));

	}

}
