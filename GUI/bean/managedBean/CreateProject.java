package managedBean;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.esprit.ejb.persistence.Project;
import com.esprit.ejb.persistence.User;
import com.esprit.ejb.services.ProjectManagementRemote;
import com.esprit.ejb.services.UserMangementRemote;

@ManagedBean(name="createProject")
@SessionScoped
public class CreateProject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	ProjectManagementRemote remote;
	
	@EJB
	UserMangementRemote remoteU;
	
private Project project=new Project();
private User user=new User();

     
public void createProject(ActionEvent event){
	
	
	//System.out.println(remoteU.getUserById(1).getFirstName());
	if(isPhpvalue1()==true){
		project.setPhpSkills("PHP");	
	}
	if(isJavavalue4()==true){
		project.setJavaSkills("JAVA");
	}
	if(isNetvalue2()==true){
		project.setNetSkills("NET");
	}if(isCvalue3()==true){
		project.setcSkills("C++");	
	} if(isSoftwaretvalue5()==true){
		project.setSoftwareSkills("SOFTWARE");	
	}if(isHardwarevalue6()==true){
		project.setHardwareSkills("HARDWARE");
		
	}
	// SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy"); 
	  DateFormat shortDf = DateFormat.getDateInstance(DateFormat.SHORT);
	project.setDeadeLine(shortDf.format(date2));
	project.setStartDate(shortDf.format(date1));
	remote.createProject(project);
	
	
	
	setNetvalue2(false);
	setCvalue3(false);
	setNetvalue2(false);
	setSoftwaretvalue5(false);
	setPhpvalue1(false);
	setHardwarevalue6(false);		
	project =new Project();
	
	date1=null;
	date2=null;
	
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	try {
		ec.redirect("Home.xhtml");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	
	
	
}
public  void getid(){
	project.setUse(new login().userlogin);
	
}

	public CreateProject() {}
	


	public Project getProject() {
		return project;
	}


	public void setProject(Project project) {
		this.project = project;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


/*************************categories*****************************/
	 private boolean phpvalue1;  
	    private boolean netvalue2; 
	    private boolean cvalue3;
	    private boolean javavalue4;
	    private boolean softwaretvalue5;
	    private boolean hardwarevalue6;
	  
	    
	  
	    public boolean isPhpvalue1() {
			return phpvalue1;
		}

		public void setPhpvalue1(boolean phpvalue1) {
			this.phpvalue1 = phpvalue1;
		}

		public boolean isNetvalue2() {
			return netvalue2;
		}

		public void setNetvalue2(boolean netvalue2) {
			this.netvalue2 = netvalue2;
		}

		public boolean isCvalue3() {
			return cvalue3;
		}

		public void setCvalue3(boolean cvalue3) {
			this.cvalue3 = cvalue3;
		}

		public boolean isJavavalue4() {
			return javavalue4;
		}

		public void setJavavalue4(boolean javavalue4) {
			this.javavalue4 = javavalue4;
		}

		public boolean isSoftwaretvalue5() {
			return softwaretvalue5;
		}

		public void setSoftwaretvalue5(boolean softwaretvalue5) {
			this.softwaretvalue5 = softwaretvalue5;
		}

		public boolean isHardwarevalue6() {
			return hardwarevalue6;
		}

		public void setHardwarevalue6(boolean hardwarevalue6) {
			this.hardwarevalue6 = hardwarevalue6;
		}

		public void addMessage() {  
	       // String summary = netvalue2 ? "Checked" : "Unchecked";  
	     String summary4 = javavalue4 ? "Checked" : "Unchecked";  
	       
	  
	     //   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
	      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary4));  
	       
	    }  
		public void addMessagephp() {  
		      String summary = phpvalue1 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		public void addMessagenet() {  
		      String summary = netvalue2 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		public void addMessagec() {  
		      String summary = cvalue3 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		public void addMessagehard() {  
		      String summary = hardwarevalue6 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		public void addMessagesoft() {  
		      String summary = softwaretvalue5 ? "Checked" : "Unchecked";  
		   
		  
		      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));  
		    
		    }  
		/*****************Date*********************/
		private Date date1;  
	      
	    private Date date2;  
	      
	    private Date date3;  
	      
	    public Date getDate1() {  
	        return date1;  
	    }  
	  
	    public void setDate1(Date date1) {  
	        this.date1 = date1;  
	    }  
	  
	    public Date getDate2() {  
	        return date2;  
	    }  
	  
	    public void setDate2(Date date2) {  
	        this.date2 = date2;  
	    }  
	      
	    public Date getDate3() {  
	        return date3;  
	    }  
	  
	    public void setDate3(Date date3) {  
	        this.date3 = date3;  
	    }  
}
