package managedBean;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import com.esprit.ejb.persistence.Project;
import com.esprit.ejb.services.ProjectManagementRemote;
import com.esprit.ejb.services.UserMangementRemote;

@ManagedBean(name = "ProjectManagement")
@SessionScoped
public class ProjectMangement implements Serializable {

	@EJB
	private UserMangementRemote remoteUser;

	@EJB
	private ProjectManagementRemote remoteChallange;

	private Project project = new Project();

	private DataModel dataModelM = new ListDataModel();

	private DataModel dataModelMid = new ListDataModel();

	public DataModel getDataModelM() {
		Object o = remoteChallange.getAllTrueProjects();

		dataModelM.setWrappedData(o);
		return dataModelM;
	}

	public void setDataModelM(DataModel dataModelM) {
		this.dataModelM = dataModelM;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	public static Project challangeid=new Project();
	 public void createSolution(ActionEvent event){
		 ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		
		 challangeid=(Project) dataModelM.getRowData();
		 try {
				ec.redirect("CreateSolution.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	 }
		}
	 public void  gotoinvite(ActionEvent event){
		 ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		
		 challangeid=(Project) dataModelM.getRowData();
		 try {
				ec.redirect("Invit.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	 }
		}
	public static Project getChallangeid() {
		return challangeid;
	}

	public static void setChallangeid(Project challangeid) {
		ProjectMangement.challangeid = challangeid;
	}

	public DataModel getDataModelMid() {
		Object o = remoteChallange.getAllTrueProjectsuser(new login().userlogin);
		dataModelM.setWrappedData(o);
		return dataModelMid;
	}

	public void setDataModelMid(DataModel dataModelMid) {
		this.dataModelMid = dataModelMid;
	}

}
