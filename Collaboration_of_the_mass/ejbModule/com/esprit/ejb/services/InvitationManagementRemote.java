package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Remote;

import com.esprit.ejb.persistence.Invitation;
import com.esprit.ejb.persistence.User;

@Remote
public interface InvitationManagementRemote {
	
	public void  createInvitation(Invitation invitation);
	public void  deleteInvitation(Invitation invitation);
	public void  updateInvitation(Invitation invitation);
	public Invitation getInvitationById(int idInvitation);
	
	public List<Invitation> getAllInvitations();
	public List <User> getAllUserInvitaionById(int id);

}
