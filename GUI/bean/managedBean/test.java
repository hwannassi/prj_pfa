package managedBean;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

import com.snowtide.pdf.OutputTarget;
import com.snowtide.pdf.PDFTextStream;

public class test {
	
	
	 public static String deserializeString(String file)
			  throws IOException {
		 FileInputStream input = new FileInputStream(file);

		 byte[] fileData = new byte[input.available()];

		 input.read(fileData);
		 input.close();

		 return new String(fileData, "UTF-8");
			  }
	 public static StringBuffer getPDFText (File pdfFile) throws IOException {
		    PDFTextStream stream = new PDFTextStream(pdfFile);
		    StringBuffer sb = new StringBuffer(1024);
		    // get OutputTarget configured to pipe text to the provided StringBuffer
		    OutputTarget tgt = OutputTarget.forBuffer(sb);
		    stream.pipe(tgt);
		    stream.close();
		    return sb;
		}
	 public static void main(String[] args) throws IOException {
		//create file object
      File file = new File("C://Atelier 1.pdf");
       System.out.println(getPDFText(file).toString());
      //  savePDFText(file,file);
        
         
         
	 }
	 public static void savePDFText (File pdfFile, File textFile) throws IOException {
		    PDFTextStream stream = new PDFTextStream(pdfFile);
		    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(textFile)));
		    // get OutputTarget configured to pipe text to the provided file path
		    OutputTarget tgt = new OutputTarget(writer);
		    stream.pipe(tgt);
		    writer.flush();
		    writer.close();
		    stream.close();
		   // System.out.println(tgt.toString());
		}

}
