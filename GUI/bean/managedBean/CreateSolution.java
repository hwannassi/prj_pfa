package managedBean;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.ServletContext;

import org.primefaces.event.RateEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.dropbox.client2.exception.DropboxException;
import com.esprit.ejb.persistence.Solution;
import com.esprit.ejb.services.SolutionManagmentRemote;
import com.esprit.ejb.services.UserMangementRemote;


@ManagedBean(name="createSolution")
@SessionScoped
public class CreateSolution<u> implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	SolutionManagmentRemote remote;
	@EJB
	UserMangementRemote remoteU;
	
	private UploadedFile uploadedFile;
	 private StreamedContent file;  
	
	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public CreateSolution() {
		// TODO Auto-generated constructor stub
	}
	
	private Solution solution =new Solution();

	
	public void createsolution(ActionEvent event) throws IOException {
	
		
		 solution.setDescription(uploadedFile.getFileName().toString()); 
		
//solution.setSolutionFile(bytes);
		remote.createSolution(solution);
		
		
	}

	public Solution getSolution() {
		return solution;
	}

	public void setSolution(Solution solution) {
		this.solution = solution;
	}
	public  void getid(){
		
		solution.setChallengee(new ProjectMangement().challangeid);
		solution.setUsers(new login().userlogin);
		System.out.println(new ProjectMangement().challangeid.getTitle());
		
	}
	 public void upload() {  
	        if(uploadedFile != null) {  
	            FacesMessage msg = new FacesMessage("Succesful", uploadedFile.getFileName() + " is uploaded.");  
	            FacesContext.getCurrentInstance().addMessage(null, msg); 
	            System.out.println(uploadedFile.getFileName().toString());
	           solution.setFilename(uploadedFile.getFileName()); 
	    		solution.setSolutionFile(uploadedFile.getContents().clone());
	          //solution.setSolutionFile(bytes);
	    		
	    
	          		remote.createSolution(solution);
	          		solution=new Solution();
	        		
	        		solution.setAcceptCondition(false);
	        		solution.setDescription("");
	        		
	        		
	        		 ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	        			try {
	        				ec.redirect("CreateSolution.xhtml");
	        			} catch (IOException e) {
	        				// TODO Auto-generated catch block
	        				e.printStackTrace();
	        			} 
	        		 
	        }  
	    }  
	 
	 private DataModel dataModel = new ListDataModel();
	 
		public DataModel getDataModel() {
			dataModel.setWrappedData(remote.getAllSolution());
		return dataModel;
	}

	public void setDataModel(DataModel dataModel) {
		this.dataModel = dataModel;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	 public void FileDownloadController() {          
	        InputStream stream = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getResourceAsStream("/images/optimusprime.jpg");  
	        file = new DefaultStreamedContent(stream, "text/txt", "document.txt");  
	    }  
	 private InputStream stream;
	 public void prepareFile(Solution solution){
		    System.out.println("Attachment: "+solution.getDescription());
		    stream = new ByteArrayInputStream(solution.getSolutionFile());
		    file = new DefaultStreamedContent(stream, "text/plain", solution.getFilename());
		    stream = null;
		}
	 
	 
	 
	 ////////////
	 private Integer rating1;  
	  
	    private Integer rating2;  
	  
	    private Integer rating3;  
	      
	    private Integer rating4 = 3;  
	    private Integer loadrating;
	  
	      
	    public void onrate(RateEvent rateEvent) {  
	        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Rate Event", "You rated:" + ((Integer) rateEvent.getRating()));  
	  
	        FacesContext.getCurrentInstance().addMessage(null, message);  
	        solution=(Solution) dataModel.getRowData();
	        
	        solution.setFeedbacks((Integer) rateEvent.getRating());
	        remote.updateSolution(solution);
	        
	    }  
	   
	    public void oncancel() {  
	        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cancel Event", "Rate Reset");  
	  
	        FacesContext.getCurrentInstance().addMessage(null, message);  
	    }  
	      
	    public Integer getRating1() {  
	        return rating1;  
	    }  
	  
	    public void setRating1(Integer rating1) {  
	        this.rating1 = rating1;  
	    }  
	  
	    public Integer getRating2() {  
	        return rating2;  
	    }  
	  
	    public void setRating2(Integer rating2) {  
	        this.rating2 = rating2;  
	    }  
	  
	    public Integer getRating3() {  
	        return rating3;  
	    }  
	  
	    public void setRating3(Integer rating3) {  
	        this.rating3 = rating3;  
	    }  
	  
	    public Integer getRating4() {  
	        return rating4;  
	    }  
	  
	    public void setRating4(Integer rating4) {  
	        this.rating4 = rating4;  
	    }

		public Integer getLoadrating() {
			return loadrating;
		}

		public void setLoadrating(Integer loadrating) {
			this.loadrating = loadrating;
		}

	////////////////////////

	
		
		public String deleteEmployee(){
			solution=(Solution) dataModel.getRowData();
			remote.deleteSolution(solution);
			dataModel.setWrappedData(remote.getAllSolution());
			return "";
		}
		
		/////////////////
	
		private String u;
		public void winner(ValueChangeEvent changeEvent){
			
		
		}
		public void testwinner(ActionEvent event){
			prepareSelectedList();
			
			
			
		}
		// map for selected stuff on the JSF page
	    private Map< Solution, Boolean >selectedUsers = new HashMap< Solution, Boolean >();
	    // users to be displayed in the table
	    private List< Solution >usersForOrgList = new ArrayList< Solution >();
	    /* this is the list that will have the selected users */
	    private Solution selectedCurfUserDTO = null;
	    /* TODO: add all the other stuff including getters and setters */
		private List<Solution> SelectedUsersList=new ArrayList();
	    private void prepareSelectedList()
	    {
	        // reset the list
	        setSelectedUsersList( new ArrayList< Solution >() );
	        for( Solution userDTO : getSelectedUsers().keySet() )
	        {
	        	
	            	// and this is the list of selected users
	        	getSelectedUsersList().clear();
	        		getSelectedUsersList().add( userDTO );
	        		for(Solution p:getSelectedUsersList()){
	    				System.out.println(p.getDescription());
	    			}
	        }
	    }

		public Map< Solution, Boolean > getSelectedUsers() {
			return selectedUsers;
		}

		public void setSelectedUsers(Map< Solution, Boolean > selectedUsers) {
			this.selectedUsers = selectedUsers;
		}

		public List< Solution > getUsersForOrgList() {
			return usersForOrgList;
		}

		public void setUsersForOrgList(List< Solution > usersForOrgList) {
			this.usersForOrgList = usersForOrgList;
		}

		public Solution getSelectedCurfUserDTO() {
			return selectedCurfUserDTO;
		}

		public void setSelectedCurfUserDTO(Solution selectedCurfUserDTO) {
			this.selectedCurfUserDTO = selectedCurfUserDTO;
		}

		public List<Solution> getSelectedUsersList() {
			return SelectedUsersList;
		}

		public void setSelectedUsersList(List<Solution> selectedUsersList) {
			SelectedUsersList = selectedUsersList;
		}
		
		
		////////////////////////////////////////////////////***************/////////////////////////
	
		 public void winnerDrop()  {
		
		   DropBox db = new DropBox();
             //File uploadFile = new File(uploadedFile);
      if (uploadedFile !=null){
    	  FacesMessage msg = new FacesMessage("Succesful", uploadedFile.getFileName() + " is uploaded .");  
          FacesContext.getCurrentInstance().addMessage(null, msg);  
            try {
            	 
			db.putFile(uploadedFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DropboxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
		
				
			}
		 public void dopage(ActionEvent event){
			 ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
				try {
					ec.redirect("Auth.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			 
			 
		 }
}
