package com.esprit.ejb.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Invitation
 *
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Invitation implements Serializable {

	
	private int idInvitation;
	@XmlIDREF
private List<User> invited;
//private User inviter;
	//private Set<Affectation> affectations;
	private Project challengeinvi;
	private static final long serialVersionUID = 1L;

	public Invitation() {
		super();
	}   
	@Id    
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdInvitation() {
		return this.idInvitation;
	}

	public void setIdInvitation(int idInvitation) {
		this.idInvitation = idInvitation;
	}
	
	@ManyToOne
	@XmlInverseReference(mappedBy="invitations")
	public Project getChallengeinvi() {
		return challengeinvi;
	}
	public void setChallengeinvi(Project challengeinvi) {
		this.challengeinvi = challengeinvi;
	}
	/*@XmlTransient
	@OneToMany(mappedBy="invitation",fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	public Set<Affectation> getAffectations() {
		return affectations;
	}
	public void setAffectations(Set<Affectation> affectations) {
		this.affectations = affectations;
	}*/
	
	@ManyToMany(fetch=FetchType.EAGER)

	public List<User> getInvited() {
		return invited;
	}
	public void setInvited(List<User> invited) {
		this.invited = invited;
	}
/*	@ManyToOne
	public User getInviter() {
		return inviter;
	}
	public void setInviter(User inviter) {
		this.inviter = inviter;
	}*/

   
}
