package com.esprit.ejb.services;

import java.util.List;

import javax.ejb.Local;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import com.esprit.ejb.persistence.User;

@Local
public interface UserMangementLocal {
	@WebMethod
	public void  createUser(User user);
	@WebMethod
	public void  deleteUser(@WebParam User user);
	@WebMethod
	public void  updateUser(User user);
	@WebMethod
	public User getUserById(int idUser);
	@WebMethod
	public List<User> getAllUser();
	

}
